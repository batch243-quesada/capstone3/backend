// dependencies
const router = require('express').Router();

// imports
const productController = require('../controllers/productController');
// const cartController = require('../controllers/cartControllers');
const auth = require('../middleware/auth');

//// ROUTES
// create products
router.post('/create', auth.verify, productController.createProduct);

// retrieve active products
router.get("/activeProducts", productController.getAllActiveProducts);

// retieve all products
router.get("/allProducts", auth.verify, productController.getAllProducts);

// retrieve single product
router.get("/:productId", productController.getSingleProduct);

// update a product
router.put("/update/:productId", auth.verify, productController.updateProduct);

// archive/unarchive product
router.patch("/archive/:productId", auth.verify, productController.archiveProduct);

// // create an order
// router.post("/:productId/add-to-cart", authen.verify, cartController.addToCart)

module.exports = router;